function refresh(){
    var elem = document.getElementById("ip");
    elem.value = "...";

    var http = new XMLHttpRequest();
    var url = "https://api.ipify.org/";
    http.open("GET", url);
    http.send();

    http.onreadystatechange = function(){
        if( this.readyState == 4 && this.status == 200 ){
            elem.value = http.responseText;
        } else {
            elem.value = "Unable to find your IP address";
        }
    }
}

function copy() {
    var elem = document.getElementById("ip");
    elem.select();
    document.execCommand("copy");
}


refresh();

// Adding listeners
document.getElementById("refresh").addEventListener("click", refresh);
document.getElementById("copy").addEventListener("click", copy);