# ShowMyIP

**Firefox browser add-on** which show your current IP address.

![Version1.1](screenshots/v1.1.png)

## Installation

You can download it from the mozilla's addons website, [link](https://addons.mozilla.org/en-US/firefox/addon/showmyip/).

## Build with

- HTML/CSS, JS

- [Ipify API](https://www.ipify.org/)

## Image

- Freepik [icon](https://www.freepik.com/free-icon/ip-address_692929.htm).

## License

This project is licensed under the MIT License.